import { 
  USERS_FETCH_DATA_SUCCESS,
  USERS_FETCH_DATA_ERROR,
  USERS_IS_LOADING
} from "../actions/actionTypes"

const initialState = {
  users: [],
  hasErrored: false,
  isLoading: false
}

export default function createReducer(state = initialState, action) {
  switch (action.type) {
    case USERS_FETCH_DATA_SUCCESS:
      return {
        ...state,
        users: action.users
      }

    case USERS_IS_LOADING:
      return {
        ...state,
        isLoading: !state.isLoading
      }

    case USERS_FETCH_DATA_ERROR:
      return {
        ...state,
        hasErrored: action.hasErrored
      }
    default: 
      return state
  }
}