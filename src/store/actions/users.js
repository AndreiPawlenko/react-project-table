import { 
  USERS_FETCH_DATA_SUCCESS,
  USERS_FETCH_DATA_ERROR,
  USERS_IS_LOADING 
} from "./actionTypes"

export function usersFetchData(url) {
  return async (dispatch) => {
    dispatch({type: USERS_IS_LOADING})

    await fetch(url)
      .then(response =>{
        if(!response.ok) throw Error(response.statusText)

        dispatch({type: USERS_IS_LOADING})

        return response
      })
      .then(response => response.json())
      .then(json => {
        dispatch({
          type: USERS_FETCH_DATA_SUCCESS,
          users: json
        })
      })
      .catch(() => {
        dispatch(usersHasErrored(true))
      })
  }
}

export function usersHasErrored(bool){
  return {
    type: USERS_FETCH_DATA_ERROR,
    hasErrored: bool
  }
}