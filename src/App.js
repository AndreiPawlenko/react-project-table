import React from 'react'
import { useSelector, useDispatch, shallowEqual } from 'react-redux'
import { usersFetchData } from './store/actions/users';
import './App.scss';
import preloader from './preloader.svg';

const dataUrl = 'https://jsonplaceholder.typicode.com/users';

const App = () => {
  const [checkedUsers, setCheckedUsers] = React.useState([]) 
  const { users, hasErrored, isLoading } = useSelector(state => state.users, shallowEqual)
  const dispatch = useDispatch()

  React.useEffect(() => {
    dispatch(usersFetchData(dataUrl))
  }, [dispatch])

  const checkHandler = id => {
    setCheckedUsers(checkedUsers.includes(id) 
      ? checkedUsers.filter(i => i !== id)
      : [...checkedUsers, id]
    )
  }

  const checkedAll = event => {
    let status = checkedUsers.length !== users.length

    if(status) event.target.checked = true

    setCheckedUsers(status ? users.map(el => el.id) : [])
  }

  if (hasErrored) 
    return <p>Sorry! There was an error loading the items</p>

  if (isLoading) 
    return <div className="preloder"><img src={preloader} alt="loading..." /></div>

  return (
    <>
      <div className="container">
        <div className="header">
          <h1>Users Table</h1>
          <div className="checkbox-all" onChange={checkedAll} checked={checkedUsers.length === users.length}>
              <label>
                Check All &nbsp;
                <input className="checkbox" type="checkbox" />
              </label>
            </div>
        </div>
        <div className="user__head">
          <div className="user__cell"> ID </div>
          <div className="user__cell"> NAME </div>
          <div className="user__cell"> EMAIL </div>
          <div className="user__cell"> PHONE </div>
          <div className="user__cell"></div>
        </div>
        {users.map(({ id, name, email, phone }) => {
          return <div key={id} className="user__row">
            <div className="user__cell"> {id} </div>
            <div className="user__cell"> {name} </div>
            <div className="user__cell"> {email} </div>
            <div className="user__cell"> {phone} </div>
            <div className="user__cell">
              <input
                className="checkbox" 
                type="checkbox" 
                onChange = {() => checkHandler(id)} 
                checked={checkedUsers.includes(id)}
              />
            </div>
          </div>
        })}
        <div className="checked-users">
          <b>Checked Users:</b> {users.filter((i) => checkedUsers.includes(i.id)).map(el => el.name).join(", ") || '...'} 
        </div>
      </div>
    </>
  )  
}

export default App;
